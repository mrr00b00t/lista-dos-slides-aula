## Como instalar as dependências
Você vai precisar de um ambiente com python na versão 3.7.x e com o gerenciador de pacotes pip
Para instalar as dependências, basta rodar "pip install -r requirements.txt"
Dependendo do seu ambiente com python (se é virtual ou nativo) você pode tentar rodar o pip
com "pip3 install -r requirements.txt"

## Como rodar os scripts
Para rodar os scripts, instale as dependências e execute o comando "python script.py"
Dependendo do seu ambiente com python (se é virtual ou nativo) você pode tentar rodar o script
com "python3 script.py"
