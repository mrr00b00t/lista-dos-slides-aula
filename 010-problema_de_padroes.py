# Variáveis de Decisão: Dicidir a quantidade y de latinhas junto com a quantidade de impressão de cada formato
# Objetivo: Maximizar o lucro
# Sujeitos à:
# Tempo de impressão da fábrica
# Limite de impressão por tamanho de folha


from docplex.mp.model import Model

model = Model('model')

# Variáveis de Decisão:
# Dicidir a quantidade y de latinhas junto com a quantidade de impressão de cada formato
x0 = model.integer_var(name='Quantidade de impressões do formato 1')
x1 = model.integer_var(name='Quantidade de impressões do formato 2')
x2 = model.integer_var(name='Quantidade de impressões do formato 3')
x3 = model.integer_var(name='Quantidade de impressões do formato 4')

y = model.integer_var(name='Quantidade de latinhas')

Tampas = 7*x0 + 3*x1 + 9*x2 + 4*x3
Corpos = 1*x0 + 2*x1 + 4*x3

# Sujeitos à:
# Tempo de impressão da fábrica
model.add_constraint(2*x0 + 3*x1 + 2*x2 + 1*x3 <= 400)

# Limite de impressão por tamanho de folha
model.add_constraint(x0 + x2 + x3 <= 200)
model.add_constraint(x1 <= 90)

# Restrição da variável auxiliar y
model.add_constraint(y <= Tampas/2)
model.add_constraint(y <= Corpos)

# Objetivo: Maximizar o lucro
model.maximize(
    106*y - 50*Corpos - 3*Tampas
)

print(model.export_to_string())

solution = model.solve(log_output=True)
solution.display()