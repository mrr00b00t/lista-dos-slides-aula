# Variáveis de Decisão: Decidir o xi número de enfermeiras que trabalham a partir do dia i
# Objetivo: Minimizar o número de enfermeiras contratadas
# Sujeitos à:
# Demanda do dia i


from docplex.mp.model import Model

model = Model('model')

Demanda = [4, 6, 2, 3, 4, 5, 7]

# Variáveis de Decisão:
# Decidir o xi número de enfermeiras que trabalham a partir do dia i
Variaveis = [model.integer_var(name='Número de enfermeiras que trabalham a partir do dia {}'.format(i+1)) for i in range(7)]

# Sujeitos à:
# Demanda do dia i
for i in range(7):
    model.add_constraint(
        model.sum(
            [Variaveis[j] for j in range(-4+1+i, i+1)]
        ) >= Demanda[i]
    )

# Objetivo: Minimizar o número de enfermeiras contratadas
model.minimize(
    model.sum(
        [Variaveis[i] for i in range(7)]
    )
)

print(model.export_to_string())

solution = model.solve(log_output=True)
solution.display()