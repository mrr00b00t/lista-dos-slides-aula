# Variáveis de Decisão: Quantidade de Gás que sai do Nó i para o Nó j
# Objetivo: Maximizar a quantidade de Gás que sai de S e vai para T
# Sujeitos à:
# Capacidade da aresta
# A quantidade de gás que entra em um nó tem que sair por esse nó, exceto para S e T

from docplex.mp.model import Model

model = Model('model')

N = 6

Capacidade = [
    [0, 11, 12, 0, 0, 0],
    [0, 0, 0, 12, 0, 0],
    [0, 1, 0, 0, 11, 0],
    [0, 0, 0, 0, 0, 19],
    [0, 0, 0, 7, 0, 4],
    [0, 0, 0, 0, 0, 0],
]

S = 0
T = 5

# Variáveis de Decisão: Quantidade de Gás que sai do Nó i para o Nó j
Variaveis = [[model.continuous_var(name='Quantidade de Gás que sai do Nó {} para o Nó {}'.format(i+1,j+1)) for j in range(N)] for i in range(N)]

# Restrições
# O fluxo de cada aresta deve ser menor ou igual que a capacidade
for i in range(N):
    for j in range(N):
        if Capacidade[i][j] != 0:
            model.add_constraint(Variaveis[i][j] <= Capacidade[i][j])

# Para cada nó, excento a fonte e o destino, a quantidade de gás que entra é igual a que sai
for k in range(N):
    if k != S and k != T:
        model.add_constraint(
            model.sum(
                [Variaveis[i][k] for i in range(N) if Capacidade[i][k] != 0]
            ) == model.sum(
                [Variaveis[k][i] for i in range(N) if Capacidade[k][i] != 0]
            )
        )

# Objetivo: Maximizar a quantidade de Gás que sai de S e vai para T
model.maximize(
    model.sum(
        [Variaveis[i][T] for i in range(N) if Capacidade[i][T] != 0]
    )
)

print(model.export_to_string())

solution = model.solve(log_output=True)
solution.display()