# Variáveis de Decisão: Decidir se o item i está ou não na mochila
# Objetivo: Maximiar a soma dos valores de cada item que está na mochila
# Sujeitos à:
# Soma do peso dos itens dentro da mochila não deve exceder a capacidade da Mochila

from docplex.mp.model import Model

model = Model('model')

N = 7
Capacidade = 50

Peso = [31, 10, 20, 19, 4, 3, 6]
Valor = [70, 20, 39, 37, 7, 5, 10]


# Variáveis de Decisão: Decidir se o item i está ou não na mochila
Variaveis = [model.binary_var(name='O item x{} está na mochila ?'.format(i)) for i in range(N)]

# Restrição:
# Soma do peso dos itens dentro da mochila não deve exceder a capacidade da Mochila
model.add_constraint(
    model.sum(
        [Variaveis[i]*Peso[i] for i in range(N)]
    ) <= Capacidade
)

# Objetivo:
# Maximiar a soma dos valores de cada item que está na mochila
model.maximize(
    model.sum(
        [Variaveis[i]*Valor[i] for i in range(N)]
    )
)

print(model.export_to_string())

solution = model.solve(log_output=True)
solution.display()