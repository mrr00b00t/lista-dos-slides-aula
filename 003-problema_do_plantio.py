# Variáveis de Decisão: Quantidade de área plantada da cultura i na Fazenda j
# Objetivo: Maximizar o lucro das plantações
# Sujeitos à:
# Área máxima de plantio pra cada cultura;
# Área máxima disponível para plantio em cada fazenda;
# Quantidade de Água disponível em cada fazenda


from docplex.mp.model import Model

model = Model('model')

FazendaArea = [400, 650, 350]
FazendaAgua = [1800, 2200, 950]

CulturaMaxArea = [660, 880, 400]
CulturaAguaPorArea = [5.5, 4, 3.5]
CulturaLucroPorArea = [5000, 4000, 1800]

# Variáveis de Decisão: Quantidade de área plantada da cultura i na fazenda j
Variaveis = [[model.continuous_var(name='Quantidade de área plantada da cultura {} na Fazenda {}'.format(i+1,j+1)) for j in range(3)] for i in range(3)]

# Restrições:
# Limitando a quantidade de plantio por Fazenda
for j in range(3):
    model.add_constraint(
        model.sum(
            [Variaveis[i][j] for i in range(3)]
        ) <= FazendaArea[j]
    )

# Limitando a quantidade de plantio por Cultura
for i in range(3):
    model.add_constraint(
        model.sum(
            [Variaveis[i][j] for j in range(3)]
        ) <= CulturaMaxArea[i]
    )

# Limitando pela quantidade de água consumida pela cultura i na fazenda j
for j in range(3):
    model.add_constraint(
        model.sum(
            [Variaveis[i][j]*CulturaAguaPorArea[i] for i in range(3)]
        ) <= FazendaAgua[j]
    )

# Objetivo: Maximizar Lucro
model.maximize(
    model.sum(
        sum(
            [[Variaveis[i][j]*CulturaLucroPorArea[i] for j in range(3)] for i in range(3)],
            []
        )
    )
)

print(model.export_to_string())

solution = model.solve(log_output=True)
solution.display()