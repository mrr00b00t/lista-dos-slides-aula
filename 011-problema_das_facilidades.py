# Variáveis de Decisão: Decidir quais depósitos serão instalados e
# quais dos clientes serão atendidos por elas
# Objetivo: Minimizar o custo de instação e atendimento
# Sujeitos à:
# Clientes só podem escolher uma fábrica
# Todos os clientes tem que ser atendidos
# Instalação dos depósitos


from docplex.mp.model import Model

model = Model('model')

N = 3
M = 4

CustoInstalacao = [4, 3, 2,]
CustoAtendimento = [
    [7, 3, 1, 13],
    [8, 9, 5, 10],
    [11, 4, 4, 9],
]

# Variáveis de Decisão:
# Decidir quais depósitos serão instalados e quais dos clientes serão atendidos por eles
Instalada = [model.binary_var(name='Fábrica {} será instalada ?'.format(i+1)) for i in range(N)]
Atende = [
    [model.binary_var(name='Depósito {} atende o cliente {}'.format(i+1,j+1)) for j in range(M)] for i in range(N)
]

# Sujeitos à:
# Clientes só podem escolher uma fábrica
# Todos os clientes tem que ser atendidos
for j in range(M):
    model.add_constraint(
        model.sum(
            [Atende[i][j] for i in range(N)]
        ) == 1
    )

# Instalação dos depósitos
for i in range(N):
    for j in range(M):
        model.add_constraint(Atende[i][j] <= Instalada[i])

# Objetivo: Minimizar o custo de instalação + o custo de atendimento
model.minimize(
    model.sum(
        [Instalada[i]*CustoInstalacao[i] for i in range(N)]
    ) + model.sum(
        sum(
            [[Atende[i][j]*CustoAtendimento[i][j] for j in range(M)] for i in range(N)],
            []
        )
    )
)

print(model.export_to_string())

solution = model.solve(log_output=True)
solution.display()