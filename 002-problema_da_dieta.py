# Variaveis de Decição: Decidir a quantidade de cada ingrediente
# Objetivo: Minimizar o custo do composto
# Sujeito à: Quantidade de Vitamina A >= 9 e Quantidade de Vitamiza C >= 19


from docplex.mp.model import Model

model = Model('model')

QuantidadeVitamina = {
    'A': [1, 0, 2, 2, 1, 2],
    'C': [0, 1, 3, 1, 3, 2]
}

Preco = [35, 30, 60, 50, 27, 22]

Variaveis = [model.integer_var(name='Quantidade do ingrediente {}'.format(i+1)) for i in range(6)]

model.add_constraint(model.sum([Variaveis[i]*QuantidadeVitamina['A'][i] for i in range(6)]) >= 9)
model.add_constraint(model.sum([Variaveis[i]*QuantidadeVitamina['C'][i] for i in range(6)]) >= 19)

model.minimize(model.sum([Variaveis[i]*Preco[i] for i in range(6)]))

print(model.export_to_string())

solution = model.solve(log_output=True)
solution.display()