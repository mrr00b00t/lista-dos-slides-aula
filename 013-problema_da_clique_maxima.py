# Variáveis de Decisão: Decidir quais vértices estão na clique máxima
# Objetivo: Maximizar a quantidade de vértices que estão na clique
# Sujeitos à:
# Não se pode escolher dois vértices que não exista uma aresta os ligando


from docplex.mp.model import Model

model = Model('model')

N = 6
Grafo = [
    [0, 1, 0, 0, 0, 1],
    [1, 0, 1, 0, 0, 0],
    [0, 1, 0, 1, 1, 1],
    [0, 0, 1, 0, 1, 0],
    [0, 0, 1, 1, 0, 1],
    [1, 0, 1, 0, 1, 0],
]

# Variáveis de Decisão: Decidir quais vértices estão na clique máxima
Variaveis = [model.binary_var(name='Nó x{} está na clique ?'.format(i)) for i in range(N)]

# Sujeitos à:
# nao se pode escolher dois vértices que não exista uma aresta os ligando
for i in range(N):
    for j in range(N):
        if Grafo[i][j] != 1 and i != j:
            model.add_constraint(
                Variaveis[i] + Variaveis[j] <= 1
            )

# Objetivo: Maximizar a quantidade de vértices que estão na clique
model.maximize(
   model.sum(
       [Variaveis[i] for i in range(N)]
   )
)

print(model.export_to_string())

solution = model.solve(log_output=True)
solution.display()