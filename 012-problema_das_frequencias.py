# Variáveis de Decisão: Decidir de a Antena i usa a cor j (é sempre possível usar N frequências para N antenas)
# Objetivo: Minimizar o uso de frequências
# Sujeitos à:
# Um nó só pode ter uma frequência
# Dois nós vizinhos não podem possuir a mesma frequência


from docplex.mp.model import Model

model = Model('model')

N = 6

Vizinhos = [
    [1, 2],
    [0, 5],
    [0, 3, 4],
    [2, 5],
    [2, 5],
    [1, 3, 4],
]

# Variáveis de Decisão:
# Decidir de a Antena i usa a cor j (é sempre possível usar N frequências para N antenas)
Variaveis = [
    [model.binary_var(name='A antena {} usa a cor {} ?'.format(i,j)) for j in range(N)] for i in range(N)
]

FrequenciaUsada = [model.binary_var(name='Cor {} é usada ?'.format(i)) for i in range(N)]

# Sujeitos à:
# Um nó só pode ter uma frequência
for i in range(N):
    model.add_constraint(
        model.sum(
            [Variaveis[i][j] for j in range(N)]
        ) == 1
    )

# Dois nós vizinhos não podem possuir a mesma frequência
for i in range(N):
    for v in Vizinhos[i]:
        for freq in range(N):
            model.add_constraint(Variaveis[i][freq] + Variaveis[v][freq] <= 1)

# Restrições da variável auxiliar que vai dizer se a frequencia j é usada
for j in range(N):
    model.add_constraint(FrequenciaUsada[j] <= 1)
    for i in range(N):
        model.add_constraint(FrequenciaUsada[j] >= Variaveis[i][j])

# Objetivo: Minimizar o uso de frequências
model.minimize(
    model.sum(
        [FrequenciaUsada[i] for i in range(N)]
    )
)

print(model.export_to_string())

solution = model.solve(log_output=True)
solution.display()