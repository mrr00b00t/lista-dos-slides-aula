# Variaveis de Decição: Decidir a quantidade de cada componente para cada tipo de tinta
# Objetivo: Minimizar o custo de produção de 1000 litros SR e 250 litros de SN
# Sujeito à: Produzir mais de 1000 litros de SR e mais de 250 litros de SN


from docplex.mp.model import Model

model = Model('model')

# Quantidade de componentes para SR
SRxSolA = model.integer_var(name='Quantidade de SolA para SR')
SRxSolB = model.integer_var(name='Quantidade de SolB para SR')
SRxSEC = model.integer_var(name='Quantidade de SEC para SR')
SRxCOR = model.integer_var(name='Quantidade de COR para SR')

# Quantidade de componentes para SN
SNxSolA = model.integer_var(name='Quantidade de SolA para SN')
SNxSolB = model.integer_var(name='Quantidade de SolB para SN')
SNxSEC = model.integer_var(name='Quantidade de SEC para SN')
SNxCOR = model.integer_var(name='Quantidade de COR para SN')


# Restrição de produção
model.add_constraint(SRxSolA+SRxSolB+SRxSEC+SRxCOR == 1000)
model.add_constraint(SNxSolA+SNxSolB+SNxSEC+SNxCOR == 250)

# Restrição de proporção
model.add_constraint(0.3*SRxSolA + 0.6*SRxSolB + SRxSEC + 0*SRxCOR >= 0.25*1000)
model.add_constraint(0.7*SRxSolA + 0.4*SRxSolB + 0*SRxSEC + SRxCOR >= 0.5*1000)

model.add_constraint(0.3*SNxSolA + 0.6*SNxSolB + SNxSEC + 0*SNxCOR >= 0.2*250)
model.add_constraint(0.7*SNxSolA + 0.4*SNxSolB + 0*SNxSEC + SNxCOR >= 0.5*250)

# Objetivo: Minimizar o custo de produção de 1000 litros SR e 250 litros de SN
model.minimize(
    1.5*(SRxSolA + SNxSolA) + 1.0*(SRxSolB + SNxSolB) + 4.0*(SRxSEC + SNxSEC) + 6.0*(SRxCOR + SNxCOR)
    )

print(model.export_to_string())

solution = model.solve(log_output=True)
solution.display()