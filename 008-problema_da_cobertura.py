# Variáveis de Decisão: Nó que estará dentro da cobertura, variável booleana
# Objetivo: Minimizar a quantidade de escolas construídas
# Sujeitos à:
# Todo deve dever ser ter uma escola ou ser vizinho de um nó que tem escola

from docplex.mp.model import Model

model = Model('model')

N = 9

Vizinhos = [
    [1, 2, 8],
    [0, 2, 3],
    [0, 1, 3, 4],
    [1, 2, 5],
    [2, 5, 6, 7],
    [3, 4, 6],
    [4, 5],
    [4, 8],
    [7, 0]
]

# Variáveis de Decisão: Nó que estará dentro da cobertura, variável booleana
Variaveis = [model.binary_var(name='Nó x{} está na cobertura?'.format(i)) for i in range(N)]

# Restrição:
# Todo deve dever ser ter uma escola ou ser vizinho de um nó que tem escola
for j in range(N):
    model.add_constraint(
        model.sum(
            sum(
                [[Variaveis[j]], [Variaveis[i] for i in Vizinhos[j]]],
                []
            )
        ) >= 1
    )

# Objetivo:
# Minimizar a quantidade de escolas construídas
model.minimize(
    model.sum(
        [Variaveis[i] for i in range(N)]
    )
)

print(model.export_to_string())

solution = model.solve(log_output=True)
solution.display()