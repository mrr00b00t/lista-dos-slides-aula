###################################
## PROBLEMA DA RAÇÃO ##############
###################################

# Preço das Rações
# AMGS = 20, RE = 30

# Preço dos Insumos
# Cereal = 1
# Carne = 4

# Fórmula das Rações
# AMGS = 5*Cereal + 1*Carne
# RE = 2*Cereal + 4*Carne

# Estoque
# 10000 Carnes e 30000 Cereais

# DEVEMOS MAXIMIZAR O LUCRO
# Lucro por fazer uma AMGS = 20 - (5*1 + 1*4) => 11
# Lucro por fazer uma RE = 30 - (2*1 + 4*4) => 12
# Váriaveis de decisão: A PRODUÇÃO, isto é, a quantidade x de AMGS e y de RE
# Devemos maximizar, então, o lucro que é x*AMGS + y*RE => max(x*11 + y*12)
# Limitado à quantidade de insumos, isto é, o total de Ceral e de Carne devem estar no estoque

# RESTRIÇÕES
# Total de Cereal: x*5 + y*2 <= 30000
# Total de Carne: x*1 + y*4 <= 10000


from docplex.mp.model import Model

model = Model('model')

x = model.integer_var(name='All Mega Giga Suprema')
y = model.integer_var(name='Ração das Estrelas ')

model.add_constraint(5*x + 2*y <= 30000)
model.add_constraint(x + 4*y <= 10000)

model.maximize(11*x + 12*y)

print(model.export_to_string())

solution = model.solve(log_output=True)
solution.display()