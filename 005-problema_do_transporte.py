# Variáveis de Decisão: Quantidade de insumo que a Fábrica i vai suprir o Depósito j
# Objetivo: Atender às demandas dos depósitos minimizando o custo de transporte Custo[i][j]
# Sujeitos à:
# Limite que cada fábrica demanda
# Limite que cada depósito pode ofertar


from docplex.mp.model import Model

model = Model('model')

Custo = [
    [8, 5, 6],
    [15, 10, 12],
    [3, 9, 10],
]

Oferta = [120, 80, 80]
Demanda = [150, 70, 60]

# Variáveis de Decisão: Quantidade de insumo que a Fábrica i vai suprir o Depósito j
Variaveis = [[model.integer_var(name='Quantidade de insumo que a Fábrica {} vai suprir o Depósito {}'.format(i+1,j+1)) for j in range(3)] for i in range(3)]

# Restrições:
# Limitando a quantidade por Fábrica
for i in range(3):
    model.add_constraint(
        model.sum(
            [Variaveis[i][j] for j in range(3)]
        ) <= Oferta[i]
    )

# Limitando a quantidade por Depósito
for j in range(3):
    model.add_constraint(
        model.sum(
            [Variaveis[i][j] for i in range(3)]
        ) >= Demanda[j]
    )

# Objetivo: Minimizar o custo de transporte
model.minimize(
    model.sum(
        sum(
            [[Variaveis[i][j]*Custo[i][j] for j in range(3)] for i in range(3)],
            []
        )
    )
)

print(model.export_to_string())

solution = model.solve(log_output=True)
solution.display()